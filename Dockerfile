FROM scratch

LABEL maintaner="Kerem Ulutas <kerem@ulutas.gen.tr>"

COPY . .

EXPOSE 8080

CMD ["./main"]
